#include <Wire.h>

#define SLAVE_ADDRESS 0x12

#define PC_Serial Serial
#define BUFF_SIZE 20

String g_buff = "";
char g_weight_buff[9] = "00000000";
int g_weight = 0;

void setup()
{
  Wire.begin(SLAVE_ADDRESS);
  delay(3000); // Helps syncronizing to the master SCL
  
  Wire.onReceive(receiveEvent);
  
  PC_Serial.begin(9600);
  PC_Serial.println(F("PC_Serial is READY."));

  g_buff.reserve(BUFF_SIZE);
}

void loop()
{
  delay(100);
}

// I2C event
void receiveEvent(int howMany)
{
  while(1 < Wire.available()) // Skip the last byte/character
  {
    char c = Wire.read();
    g_buff += c;
  }
  
  char c = Wire.read();    // Process the last remaining byte/character

  String msg = "ST,GS,"; // g_buff[g_buff.length()-1] == '1'
  
  if (g_buff[g_buff.length()-1] == '2')
  {
    msg = "US,GS,";
  }
  
  msg = msg + g_buff.substring(0, g_buff.length()-1) + ",kg";
  PC_Serial.println(msg);
  g_buff = "";
}
