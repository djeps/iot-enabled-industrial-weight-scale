#include <UbidotsCC3200.h>
#include <WiFi.h>
#include <Wire.h>

#define DEBUG_Serial Serial
#define I2C_Bus Wire

// ---------------------------------------
// Set your WiFi's connection 'properties'
// ---------------------------------------
namespace
{  
  char SSID_NAME[] = "SSID_NAME"; // Your WiFi SSID
  char SSID_PASS[] = "SSID_PASS"; // Your WiFi password
  const char * TOKEN = "BBFF-token"; // Your Ubidots TOKEN
  const char * VARIABLE_LABEL= "f_weight_scale_kg"; // Your Ubidots variable label
}

Ubidots client(TOKEN); // Initialize the Ubidots instance

const int PACKET_SIZE = 10;

const int SCALE_SLAVE_ADDR = 0x11;
const int PC_SLAVE_ADDR = 0x12;

// ---------------
// Pin assignments
// ---------------
int LED_G = PE_0;
int LED_Y = PA_5;

bool g_packet_end = false;
String g_packet = "";
String gs_weight = "";

void initGlobals();
void readFromScale ( int _numOfBytes );
void sendToPc ( String _msg );
void uploadWeight ( String _msg );
void printWifiData();
void printCurrentNet();

void setup()
{
  // ------------------------
  // Set and init output pins
  // ------------------------
  pinMode(LED_G, OUTPUT); // LED_G - green LED. Turns ON when connected to the WiFi
  pinMode(LED_Y, OUTPUT); // LED_Y - yellow LED. Toggles ON/OFF when receiving data on UART5
  digitalWrite(LED_G, LOW);
  digitalWrite(LED_Y, LOW);
  
  initGlobals();
  
  DEBUG_Serial.begin(9600);
  I2C_Bus.begin();
  
  DEBUG_Serial.println(F("Master device READY."));
  
  g_packet.reserve(PACKET_SIZE);

  // delay() is blocking code and is intentionally put inside the setup() function
  
  delay(5000); // Helps the slave syncronize to the SCL

  // ---------------------------------------------------------
  // Attempt at establishing a connection to your WiFi network
  // ---------------------------------------------------------
  Serial.print("\nAttempting to connect to Network named: ");
  Serial.println(SSID_NAME);
  
  WiFi.begin(SSID_NAME, SSID_PASS);
  
  while ( WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(300);
  }
  
  Serial.println("\n\nYou're connected to the network.");
  Serial.println("Waiting for an ip address");

  // -----------------------------------------------
  // Wait until a valid IP address has been assigned
  // -----------------------------------------------
  while (WiFi.localIP() == INADDR_NONE)
  {
    Serial.print(".");
    delay(300);
  }

  // ---------------------------------------------------------
  // Toggle the GREEN LED once an IP address has been obtained
  // ---------------------------------------------------------
  Serial.println("\n\nIP Address obtained!");
  digitalWrite(LED_G, HIGH);

  // -------------------------------------------
  // Print out the status of the WiFi connection
  // -------------------------------------------
  printCurrentNet();
  printWifiData();
  Serial.println();
  Serial.println("READY.\n");

  client.setDeviceLabel("tm4c123gxl");
  client.setDebug(false);
}

void loop()
{
  readFromScale(9);

  // TODO: remove blocking code 'delay'
  delay(1000);

  sendToPc(g_packet);
  
  // TODO: remove blocking code 'delay'
  delay(1000);
  
  uploadWeight(g_packet);

  // TODO: remove blocking code 'delay'
  delay(5000);
  
  initGlobals();

  // TODO: remove blocking code 'delay'
  delay(1000);
}

void readFromScale ( int _numOfBytes )
{
  g_packet_end = false;

  I2C_Bus.requestFrom(SCALE_SLAVE_ADDR, _numOfBytes);

  while ( I2C_Bus.available() )
  {
    digitalWrite(LED_Y, HIGH);
    char c = I2C_Bus.read();
    g_packet += c;
  }
  
  DEBUG_Serial.print(F("Got from SCALE slave device, weight: "));
  gs_weight = g_packet.substring(0, g_packet.length()-1);
  DEBUG_Serial.print(gs_weight);
  DEBUG_Serial.println(" kg");

  g_packet_end = true;
  digitalWrite(LED_Y, LOW);
}

void sendToPc ( String _msg )
{  
  char charBuff[PACKET_SIZE];

  for(int i=0; i<PACKET_SIZE; i++)
  {
    charBuff[i] = _msg[i];
  }
  charBuff[PACKET_SIZE-1] = ' '; // An extra byte I'm sacrificing. Without this I was experiencing problems!?
  
  
  if ( g_packet_end )
  {
    I2C_Bus.beginTransmission(PC_SLAVE_ADDR);
    I2C_Bus.write(charBuff);
    I2C_Bus.endTransmission();
    
    DEBUG_Serial.print(F("Sent to PC slave device: "));
    DEBUG_Serial.println(_msg);

    g_packet = "";
  }
}

void uploadWeight ( String _msg )
{
  if ( g_packet_end )
  {
    float f_weight = gs_weight.toFloat();
    DEBUG_Serial.print("f_weight = ");
    DEBUG_Serial.print(f_weight);
    DEBUG_Serial.println(" kg");
    client.add(VARIABLE_LABEL, f_weight);
    client.sendAll();
  }
}

void initGlobals()
{
  if ( g_packet_end )
  {
    g_packet_end = false;
    g_packet = "";
  }
}

void printWifiData()
{
  // Print your WiFi IP address:
  IPAddress ip = WiFi.localIP();
  DEBUG_Serial.print("IP Address: ");
  DEBUG_Serial.println(ip);
}

void printCurrentNet()
{
  // Print the SSID_NAME of the network you're attached to:
  DEBUG_Serial.print("SSID_NAME: ");
  DEBUG_Serial.println(WiFi.SSID());

  // Print the received signal strength:
  long rssi = WiFi.RSSI();
  DEBUG_Serial.print("Signal strength (RSSI):");
  DEBUG_Serial.println(rssi);
}
