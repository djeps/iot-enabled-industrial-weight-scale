# IoT Enabled Industrial Weight Scale

This is just a crude attempt at exploring IoT, by connecting an industrial weighing scale
to the Internet and the Ubidots platform - for simple monitoring and alarm notification.

The way it's meant to work is to 'cut' the existing connection between the weight scale
indicator and the PC, and introduce the new device.

Then connect the scale indicator directly to the device and route whatever comes on it's
serial port to the secondary one so that the PC software can continue operating unaware of
what's happening in-between.
