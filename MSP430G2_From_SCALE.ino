#include <Wire.h>

#define SLAVE_ADDRESS 0x11

#define SCALE_Serial Serial
#define BUFF_SIZE 20

String g_buff = "";
char g_weight_buff[10] = "       01";
unsigned int g_weight = 0;

void setup()
{
  Wire.begin(SLAVE_ADDRESS);
  Wire.onRequest(requestEvent);

  SCALE_Serial.begin(9600);
  SCALE_Serial.println(F("SCALE_Serial is READY."));

  g_buff.reserve(BUFF_SIZE);
}

void loop()
{  
  SCALE_Serial.print(F("Weight = "));
  SCALE_Serial.print(g_weight);
  SCALE_Serial.println(F(" kg"));
  
  delay(1000);
}

// UART event
void serialEvent()
{
  SCALE_Serial.print("FROM UART: ");

  while (SCALE_Serial.available())
  {
    char c = (char)SCALE_Serial.read();
    g_buff += c;
  }
  
  if (!SCALE_Serial.available())
  {
    SCALE_Serial.println(g_buff);
    String weight = g_buff.substring(6, 14);
    
    g_weight = weight.toInt();

    // !!! FOR WHATEVER REASON, toCharArray IS NOT PRODUCING THE DESIRED RESULTS !!!
    //g_buff.substring(6, 14).toCharArray(g_weight_buff, 8);
    for(int i=6; i<15; i++)
    {
      g_weight_buff[i-6] = g_buff[i];
    }
    
    if (g_buff[0] == 'S')
    {
      g_weight_buff[8] = '1'; // An indicator of the starting string in the messsage i.e. 'ST,GS'
      //SCALE_Serial.println("'S' detected!");
    }
    else
    {
      g_weight_buff[8] = '2'; // or 'US,GS'
      //SCALE_Serial.println("'U' detected!");
      
    }
    
    //SCALE_Serial.print("g_weight_buff = ");
    //SCALE_Serial.println(g_weight_buff);
    
    g_buff = "";
  }
}

// I2C event
void requestEvent()
{
  Wire.write(g_weight_buff);
}
